package com.ushakov.tm.controller;

import com.ushakov.tm.api.ICommandController;
import com.ushakov.tm.api.ICommandService;
import com.ushakov.tm.model.Command;
import com.ushakov.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Ivan Ushakov");
        System.out.println("E-MAIL: iushakov@tsconsulting.com \n");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0 \n");
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) System.out.println(command.getName());
        System.out.println();
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands)
            if (command.getArg() != null) System.out.println(command.getArg());
        System.out.println();
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            final String name = command.getName();
            if (name == null) continue;
            System.out.println(command);
        }
        System.out.println();
    }

    @Override
    public void showSystemInfo() {
        System.out.println("[SYSTEM INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " +  NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
        System.out.println();
    }

    @Override
    public void exit() {
        System.exit(0);
    }

}
