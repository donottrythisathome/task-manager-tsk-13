package com.ushakov.tm.controller;

import com.ushakov.tm.api.IProjectTaskService;
import com.ushakov.tm.api.ITaskController;
import com.ushakov.tm.api.ITaskService;
import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.service.TaskService;
import com.ushakov.tm.util.TerminalUtil;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    private final IProjectTaskService projectTaskService;

    public TaskController(ITaskService taskService, IProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showList() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK] \n");
    }

    @Override
    public void create() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String taskDescription = TerminalUtil.nextLine();
        final Task task = taskService.add(taskName, taskDescription);
        if (task == null) {
            System.out.println("[FAILED] \n");
            return;
        }
        System.out.println("[OK] \n");
    }

    @Override
    public void clear() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK] \n");
    }

    @Override
    public void removeTaskById() {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(taskId);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY REMOVED \n");
        System.out.println();
    }

    @Override
    public void removeTaskByName() {
        System.out.println("ENTER TASK NAME");
        final String taskName = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(taskName);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY REMOVED \n");
        System.out.println();
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        if (taskIndex < 1) {
            System.out.println("INCORRECT INDEX \n");
            return;
        }
        final Task task = taskService.removeOneByIndex(taskIndex - 1);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY REMOVED \n");
    }

    @Override
    public void findTaskById() {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(taskId);
        if (task == null) {
            System.out.println("NOT FOUND \n");
            return;
        }
        System.out.println(task);
        System.out.println();
    }

    @Override
    public void findTaskByName() {
        System.out.println("ENTER TASK NAME");
        final String taskName = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(taskName);
        if (task == null) {
            System.out.println("NOT FOUND \n");
            return;
        }
        System.out.println(task);
        System.out.println();
    }

    @Override
    public void findTaskByIndex() {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        if (taskIndex < 1) {
            System.out.println("INCORRECT INDEX \n");
            return;
        }
        final Task task = taskService.findOneByIndex(taskIndex - 1);
        if (task == null) {
            System.out.println("NOT FOUND \n");
            return;
        }
        System.out.println(task);
        System.out.println();
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        if (taskIndex < 1) {
            System.out.println("INCORRECT INDEX \n");
            return;
        }
        final Task task = taskService.findOneByIndex(taskIndex - 1);
        if (task == null) {
            System.out.println("TASK NOT FOUND \n");
            return;
        }
        System.out.println("ENTER NAME");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String taskDescription = TerminalUtil.nextLine();
        if (taskService.updateTaskByIndex(taskIndex - 1, taskName, taskDescription) == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY UPDATED \n");
    }

    @Override
    public void updateTaskById() {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(taskId);
        if (task == null) {
            System.out.println("TASK NOT FOUND \n");
            return;
        }
        System.out.println("ENTER NAME");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String taskDescription = TerminalUtil.nextLine();
        if (taskService.updateTaskById(taskId, taskName, taskDescription) == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY UPDATED \n");
    }

    @Override
    public void startTaskById() {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.startTaskById(taskId);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY STARTED \n");
        System.out.println();
    }

    @Override
    public void startTaskByName() {
        System.out.println("ENTER TASK NAME");
        final String taskName = TerminalUtil.nextLine();
        final Task task = taskService.startTaskByName(taskName);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY STARTED \n");
        System.out.println();
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        if (taskIndex < 1) {
            System.out.println("INCORRECT INDEX \n");
            return;
        }
        final Task task = taskService.startTaskByIndex(taskIndex - 1);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY STARTED \n");
    }

    @Override
    public void completeTaskById() {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.completeTaskById(taskId);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY COMPLETED \n");
        System.out.println();
    }

    @Override
    public void completeTaskByName() {
        System.out.println("ENTER TASK NAME");
        final String taskName = TerminalUtil.nextLine();
        final Task task = taskService.completeTaskByName(taskName);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY COMPLETED \n");
        System.out.println();
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        if (taskIndex < 1) {
            System.out.println("INCORRECT INDEX \n");
            return;
        }
        final Task task = taskService.completeTaskByIndex(taskIndex - 1);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY COMPLETED \n");
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeTaskStatusById(taskId, status);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("STATUS IS SUCCESSFULLY UPDATED \n");
        System.out.println();
    }

    @Override
    public void changeTaskStatusByName() {
        System.out.println("ENTER TASK NAME");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeTaskStatusByName(taskName, status);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("STATUS IS SUCCESSFULLY UPDATED \n");
        System.out.println();
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        if (taskIndex < 1) {
            System.out.println("INCORRECT INDEX \n");
            return;
        }
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeTaskStatusByIndex(taskIndex-1, status);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("STATUS IS SUCCESSFULLY UPDATED \n");
        System.out.println();
    }

    @Override
    public void findAllTasksByProjectId() {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> projectTaskList = projectTaskService.findAllTasksByProjectId(projectId);
        if (projectTaskList == null) {
            System.out.println("FAILED \n");
            return;
        }
        int index = 1;
        for (final Task task: projectTaskList) {
            System.out.println(index + ": " +task);
            System.out.println();
            index++;
        }
    }

    @Override
    public void bindTaskByProjectId() {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.bindTaskByProjectId(projectId, taskId);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("TASK WAS SUCCESSFULLY BOUND \n");
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindTaskFromProject(taskId);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("TASK WAS SUCCESSFULLY UNBOUND \n");
    }

}
