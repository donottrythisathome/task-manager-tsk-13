package com.ushakov.tm.api;

import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    void add(Project project);

    void remove(Project project);

    void clear();

    Project removeOneByName(String name);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project findOneByName(String name);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

}
