package com.ushakov.tm.api;

import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    // TASK CONTROLLER
    List<Task> findAllTasksByProjectId(String projectId);

    // TASK CONTROLLER
    Task bindTaskByProjectId(String projectId, String taskId);

    // TASK CONTROLLER
    Task unbindTaskFromProject(String taskId);

    // PROJECT CONTROLLER
    Project deleteProjectById(String projectId);

}
