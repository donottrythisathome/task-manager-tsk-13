package com.ushakov.tm.bootstrap;

import com.ushakov.tm.api.*;
import com.ushakov.tm.constant.ArgumentConst;
import com.ushakov.tm.constant.TerminalConst;
import com.ushakov.tm.controller.CommandController;
import com.ushakov.tm.controller.ProjectController;
import com.ushakov.tm.controller.TaskController;
import com.ushakov.tm.repository.CommandRepository;
import com.ushakov.tm.repository.ProjectRepository;
import com.ushakov.tm.repository.TaskRepository;
import com.ushakov.tm.service.CommandService;
import com.ushakov.tm.service.ProjectService;
import com.ushakov.tm.service.ProjectTaskService;
import com.ushakov.tm.service.TaskService;
import com.ushakov.tm.util.TerminalUtil;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ITaskController taskController = new TaskController(taskService, projectTaskService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    public void run(final String... args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        while(true) {
            System.out.println("ENTER COMMAND");
            final String command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_ABOUT: commandController.showAbout(); break;
            case ArgumentConst.ARG_VERSION: commandController.showVersion(); break;
            case ArgumentConst.ARG_HELP: commandController.showHelp(); break;
            case ArgumentConst.ARG_INFO: commandController.showSystemInfo(); break;
            default: showIncorrectArgument();
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_ABOUT: commandController.showAbout(); break;
            case TerminalConst.CMD_VERSION: commandController.showVersion(); break;
            case TerminalConst.CMD_HELP: commandController.showHelp(); break;
            case TerminalConst.CMD_INFO: commandController.showSystemInfo(); break;
            case TerminalConst.CMD_COMMANDS: commandController.showCommands(); break;
            case TerminalConst.CMD_ARGUMENTS: commandController.showArguments(); break;
            case TerminalConst.CMD_TASK_LIST: taskController.showList(); break;
            case TerminalConst.CMD_TASK_CREATE: taskController.create(); break;
            case TerminalConst.CMD_TASK_CLEAR: taskController.clear(); break;
            case TerminalConst.CMD_FIND_TASK_BY_ID: taskController.findTaskById(); break;
            case TerminalConst.CMD_FIND_TASK_BY_NAME: taskController.findTaskByName(); break;
            case TerminalConst.CMD_FIND_TASK_BY_INDEX: taskController.findTaskByIndex(); break;
            case TerminalConst.CMD_REMOVE_TASK_BY_ID: taskController.removeTaskById(); break;
            case TerminalConst.CMD_REMOVE_TASK_BY_NAME: taskController.removeTaskByName(); break;
            case TerminalConst.CMD_REMOVE_TASK_BY_INDEX: taskController.removeTaskByIndex(); break;
            case TerminalConst.CMD_START_TASK_BY_ID: taskController.startTaskById(); break;
            case TerminalConst.CMD_START_TASK_BY_NAME: taskController.startTaskByName(); break;
            case TerminalConst.CMD_START_TASK_BY_INDEX: taskController.startTaskByIndex(); break;
            case TerminalConst.CMD_COMPLETE_TASK_BY_ID: taskController.completeTaskById(); break;
            case TerminalConst.CMD_COMPLETE_TASK_BY_NAME: taskController.completeTaskByName(); break;
            case TerminalConst.CMD_COMPLETE_TASK_BY_INDEX: taskController.completeTaskByIndex(); break;
            case TerminalConst.CMD_CHANGE_TASK_STATUS_BY_ID: taskController.changeTaskStatusById(); break;
            case TerminalConst.CMD_CHANGE_TASK_STATUS_BY_NAME: taskController.changeTaskStatusByName(); break;
            case TerminalConst.CMD_CHANGE_TASK_STATUS_BY_INDEX: taskController.changeTaskStatusByIndex(); break;
            case TerminalConst.CMD_UPDATE_TASK_BY_ID: taskController.updateTaskById(); break;
            case TerminalConst.CMD_UPDATE_TASK_BY_INDEX: taskController.updateTaskByIndex(); break;
            case TerminalConst.CMD_FIND_TASKS_BY_PROJECT_ID: taskController.findAllTasksByProjectId(); break;
            case TerminalConst.CMD_BIND_TASK_BY_PROJECT_ID: taskController.bindTaskByProjectId(); break;
            case TerminalConst.CMD_UNBIND_TASK_FROM_PROJECT: taskController.unbindTaskFromProject(); break;
            case TerminalConst.CMD_PROJECT_LIST: projectController.showList(); break;
            case TerminalConst.CMD_PROJECT_CREATE: projectController.create(); break;
            case TerminalConst.CMD_PROJECT_CLEAR: projectController.clear(); break;
            case TerminalConst.CMD_FIND_PROJECT_BY_ID: projectController.findProjectById(); break;
            case TerminalConst.CMD_FIND_PROJECT_BY_NAME: projectController.findProjectByName(); break;
            case TerminalConst.CMD_FIND_PROJECT_BY_INDEX: projectController.findProjectByIndex(); break;
            case TerminalConst.CMD_REMOVE_PROJECT_BY_ID: projectController.removeProjectById(); break;
            case TerminalConst.CMD_REMOVE_PROJECT_BY_NAME: projectController.removeProjectByName(); break;
            case TerminalConst.CMD_REMOVE_PROJECT_BY_INDEX: projectController.removeProjectByIndex(); break;
            case TerminalConst.CMD_UPDATE_PROJECT_BY_ID: projectController.updateProjectById(); break;
            case TerminalConst.CMD_UPDATE_PROJECT_BY_INDEX: projectController.updateProjectByIndex(); break;
            case TerminalConst.CMD_START_PROJECT_BY_ID: projectController.startProjectById(); break;
            case TerminalConst.CMD_START_PROJECT_BY_NAME: projectController.startProjectByName(); break;
            case TerminalConst.CMD_START_PROJECT_BY_INDEX: projectController.startProjectByIndex(); break;
            case TerminalConst.CMD_COMPLETE_PROJECT_BY_ID: projectController.completeProjectById(); break;
            case TerminalConst.CMD_COMPLETE_PROJECT_BY_NAME: projectController.completeProjectByName(); break;
            case TerminalConst.CMD_COMPLETE_PROJECT_BY_INDEX: projectController.completeProjectByIndex(); break;
            case TerminalConst.CMD_CHANGE_PROJECT_STATUS_BY_ID: projectController.changeProjectStatusById(); break;
            case TerminalConst.CMD_CHANGE_PROJECT_STATUS_BY_NAME: projectController.changeProjectStatusByName(); break;
            case TerminalConst.CMD_CHANGE_PROJECT_STATUS_BY_INDEX: projectController.changeProjectStatusByIndex(); break;
            case TerminalConst.CMD_DELETE_PROJECT_WITH_TASKS: projectController.deleteProjectById(); break;
            case TerminalConst.CMD_EXIT: commandController.exit(); break;
            default: showIncorrectCommandMessage();
        }
    }

    public static void showIncorrectCommandMessage() {
        System.out.println("Error! Command is not correct...\n");
    }

    public static void showIncorrectArgument() {
        System.out.println("Error! Argument is not correct...\n");
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}
