package com.ushakov.tm.service;

import com.ushakov.tm.api.IProjectRepository;
import com.ushakov.tm.api.IProjectTaskService;
import com.ushakov.tm.api.ITaskRepository;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private ITaskRepository taskRepository;

    private IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findAllTasksByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task bindTaskByProjectId(String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        final Project project = projectRepository.findOneById(projectId);
        if (project == null) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(String taskId) {
        if (taskId == null || taskId.isEmpty()) return null;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public Project deleteProjectById(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        final List<Task> taskList = taskRepository.findAllByProjectId(projectId);
        for (final Task task: taskList) {
            taskRepository.removeOneById(task.getId());
        }
        return projectRepository.removeOneById(projectId);
    }

}
